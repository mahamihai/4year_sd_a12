package servlets;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;
import java.io.IOException;
import java.io.PrintWriter;

@WebFilter("/*")
public class MyFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.print("Working");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String path = ((HttpServletRequest) request).getRequestURI();
        System.out.println(path);
        PrintWriter writer=response.getWriter();

        if (path.equals("/A12/login")) {

            System.out.println("This is a login");

            chain.doFilter(request, response); // Just continue chain.
        } else {
            System.out.println("This is not a login");

            HttpSession session=((HttpServletRequest) request).getSession();
            if(session.getAttribute("Username")!=null || session.getAttribute("Password") != null)
            {
                System.out.println("You are OK");

                chain.doFilter(request, response); // Just continue chain.

            }
            else {
                System.out.println("I've blocked your request");

             ((HttpServletResponse )response).sendRedirect("/A12/login");

            }
            // Do your business stuff here for all paths other than /specialpath.
        }

    }

    @Override
    public void destroy() {
        System.out.println("Working");

    }
}
