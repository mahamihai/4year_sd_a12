package servlets;

import BLL.LoginService;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "LoginServlet", urlPatterns = {"login"}, loadOnStartup = 1)
public class LoginServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("Here");

        request.getRequestDispatcher("login.jsp").forward(request, response);

    }
    private LoginService _loginService=new LoginService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("In the post");
        String username = request.getParameter("Username");
        String password = request.getParameter("Password");
        PrintWriter writer=response.getWriter();
        if ((username == null)  || (password==null))
        {
            writer.print("<h1> Wrong credentials</h1>");
        }
        else
        {
            if(this._loginService.checkLogin(username,password).equals("Admin"))
            {
                HttpSession session=((HttpServletRequest) request).getSession(true);
                session.setAttribute("Username",username);
                session.setAttribute(
                        "Password",password
                );
                System.out.println("Session set");



            }
        }
       writer.print("<div> Logged with" +
               username+" "+password
       +"</div>");
    }
}